import countries from '../data/countries.json';

const defaultState = {
    countries
};

export default function staticData(state = defaultState) {
    return state;
}
