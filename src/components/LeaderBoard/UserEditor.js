import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {isEmpty} from 'lodash';
import {
    Container,
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Input,
    Label,
    Spinner
} from 'reactstrap';
import {faUserEdit} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {requestSaveUser} from '../../actions/user';


class UserEditor extends Component {
    static propTypes = {
        requestSaveUser: PropTypes.func.isRequired,
        countries: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
        user: PropTypes.shape({}),
        saving: PropTypes.bool
    };
    static defaultProps = {
        user: {},
        saving: false
    };

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            user: this.props.user
        };
    }

    componentDidUpdate(prevProps) {
        if (this.props.saving === false && prevProps.saving === true) {
            // eslint-disable-next-line react/no-did-update-set-state
            this.setState(() => ({modal: false}));
        }
    }

    modalToggle() {
        this.setState(state => ({
            modal: !state.modal
        }));
    }

    handleChange({target: {id, value}}) {
        this.setState(
            state => ({
                user: {...state.user, [id]: value}
            })
        );
    }

    handleSubmit() {
        this.props.requestSaveUser(this.state.user);
    }


    render() {
        const {user} = this.state;
        return (
            <Container>
                {isEmpty(this.props.user) ?
                    <Button color="primary" onClick={() => this.modalToggle()}>Add New Player</Button>
                    : <FontAwesomeIcon onClick={() => this.modalToggle()} icon={faUserEdit}/>}

                <Modal isOpen={this.state.modal} toggle={() => this.modalToggle()}>
                    <ModalHeader toggle={() => this.modalToggle()}>
                        {isEmpty(this.props.user) ? 'New User' : 'Edit User'}
                    </ModalHeader>
                    <ModalBody>
                        {this.props.saving ? <Spinner type="grow" color="primary"/> :
                            // eslint-disable-next-line react/jsx-indent
                            <Form>
                                <FormGroup>
                                    <Label for="name">User Name</Label>
                                    <Input
                                        onChange={e => this.handleChange(e)}
                                        type="text"
                                        name="name"
                                        id="name"
                                        value={user.name ? user.name : ''}
                                        placeholder="User Name"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="earnings">Earnings</Label>
                                    <Input
                                        onChange={e => this.handleChange(e)}
                                        type="number"
                                        name="earnings"
                                        id="earnings"
                                        value={user.earnings ? user.earnings : ''}
                                        placeholder="Earnings"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="country">Country</Label>
                                    <Input
                                        onChange={e => this.handleChange(e)}
                                        type="select"
                                        name="country"
                                        id="country"
                                        value={user.country ? user.country : ''}

                                    >
                                        <option>Select Country</option>
                                        {this.props.countries.map(country => (
                                            <option key={country.code} value={country.code}>{country.name}</option>
                                        ))}
                                    </Input>
                                </FormGroup>
                            </Form>}
                    </ModalBody>
                    <ModalFooter>
                        <Button
                            color="primary"
                            onClick={() => this.handleSubmit()}
                            disabled={!user.country || !user.name || !user.earnings}
                        >Submit
                        </Button>{' '}
                        <Button color="secondary" onClick={() => this.modalToggle()}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </Container>
        );
    }
}

export default connect(
    state => ({
        saving: state.user.saving,
        countries: state.staticData.countries
    }),
    dispatch => ({
        requestSaveUser: user => dispatch(requestSaveUser(user))
    })
)(UserEditor);
