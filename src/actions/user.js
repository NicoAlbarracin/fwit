export const USER_SAVE_REQUESTED = 'USER_SAVE_REQUESTED';
export const USER_SAVE_SUCCEEDED = 'USER_SAVE_SUCCEEDED';

export const requestSaveUser = user => ({
    type: USER_SAVE_REQUESTED,
    user
});

export const notifyUserSavedSuccessfully = () => ({
    type: USER_SAVE_SUCCEEDED
});

export const USERS_FETCH_REQUESTED = 'USERS_FETCH_REQUESTED';
export const USERS_FETCH_SUCCEEDED = 'USERS_FETCH_SUCCEEDED';

export const requestUsers = () => ({
    type: USERS_FETCH_REQUESTED
});

export const receiveUsers = users => ({
    type: USERS_FETCH_SUCCEEDED,
    users
});
