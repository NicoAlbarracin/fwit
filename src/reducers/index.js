import {combineReducers} from 'redux';

import error from './error';
import staticData from './staticData';
import user from './user';

export default combineReducers({
    error,
    user,
    staticData
});
