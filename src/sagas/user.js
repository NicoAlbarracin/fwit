import {call, put} from 'redux-saga/effects';
import {anErrorOccurred, clearError} from '../actions';

import {notifyUserSavedSuccessfully, receiveUsers} from '../actions/user';

import UserService from '../services/user';

export function* saveUser({user}) {
    yield put(clearError());
    const {error} = yield call(UserService.save, user);
    if (error) {
        yield put(anErrorOccurred({anErrorOccurred: true, errorMsg: error}));
    } else {
        const {users} = yield call(UserService.fetch);
        yield put(receiveUsers(users));
        yield put(notifyUserSavedSuccessfully());
    }
}

export function* fetchUsers() {
    yield put(clearError());
    const {error, users} = yield call(UserService.fetch);
    if (error) {
        yield put(anErrorOccurred({anErrorOccurred: true, errorMsg: error}));
    } else {
        yield put(receiveUsers(users));
    }
}
