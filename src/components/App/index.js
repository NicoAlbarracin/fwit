import React, {Fragment} from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom';

import LeaderBoard from '../LeaderBoard';

const App = () => (
    <BrowserRouter>
        <Fragment>
            <main>
                <Switch>
                    <Route exact path="/leaderboard" component={LeaderBoard}/>
                    <Redirect from="/" to="/leaderboard"/>
                </Switch>
            </main>
        </Fragment>
    </BrowserRouter>
);

export default App;
