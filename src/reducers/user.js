import {
    USER_SAVE_REQUESTED,
    USER_SAVE_SUCCEEDED,
    USERS_FETCH_REQUESTED,
    USERS_FETCH_SUCCEEDED

} from '../actions/user';

export default function user(state = {}, action) {
    switch (action.type) {
        case USERS_FETCH_REQUESTED:
            return {...state, loading: true};
        case USERS_FETCH_SUCCEEDED:
            return {...state, users: action.users, loading: false};
        case USER_SAVE_REQUESTED:
            return {...state, saving: true};
        case USER_SAVE_SUCCEEDED:
            return {...state, saving: false};
        default:
            return state;
    }
}
