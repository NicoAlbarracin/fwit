import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Table, Container} from 'reactstrap';
import {connect} from 'react-redux';
import ReactCountryFlag from 'react-country-flag';

import {requestUsers} from '../../actions/user';
import UserEditor from './UserEditor';
import {formatNumber} from '../../util';


class LeaderBoard extends Component {
    static propTypes = {
        requestUsers: PropTypes.func.isRequired,
        users: PropTypes.arrayOf(PropTypes.shape({})),
        user: PropTypes.shape({})
    };

    static defaultProps = {
        users: [],
        user: {}
    };

    componentDidMount() {
        this.props.requestUsers();
    }

    renderTable() {
        return (
            <tbody>
                {this.props.users.map((user, index) => (
                    <tr key={user._id}>
                        <th scope="row">{index + 1}</th>
                        <td>{user.name}</td>
                        <td>{'$'}{formatNumber(user.earnings)}</td>
                        <td><ReactCountryFlag code={user.country} svg/>{user.country}</td>
                        <td><UserEditor user={user}/></td>
                    </tr>))}
            </tbody>
        );
    }


    render() {
        return (
            <Container>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Player</th>
                            <th>Winnings</th>
                            <th>Native Of</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    {this.props.users && this.renderTable()}
                </Table>
                <UserEditor/>
            </Container>

        );
    }
}

export default connect(
    state => ({
        users: state.user.users,
        loading: state.user.loading
    }),
    dispatch => ({
        requestUsers: () => dispatch(requestUsers())
    })
)(LeaderBoard);

