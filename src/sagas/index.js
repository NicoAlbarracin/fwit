import {all, takeEvery} from 'redux-saga/effects';
import {USER_SAVE_REQUESTED, USERS_FETCH_REQUESTED} from '../actions/user';
import {saveUser, fetchUsers} from './user';


export default function* root() {
    yield all([
        takeEvery(USER_SAVE_REQUESTED, saveUser),
        takeEvery(USERS_FETCH_REQUESTED, fetchUsers)
    ]);
}
