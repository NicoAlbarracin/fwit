import Http from './http';


const API = 'public-api/users/';

export default class UserService {
    static async fetch() {
        const {users} = await Http.get(`${API}`);
        return {users};
    }
    static async save(user) {
        if (user._id) {
            return Http.put(`${API}${user._id}`, {user});
        }
        return Http.post(`${API}`, {user});
    }
}
